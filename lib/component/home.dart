import 'package:api_productostia/repository/prod_data.dart';
import 'package:api_productostia/screen/list_screen.dart';
import 'package:flutter/material.dart';

class home extends StatelessWidget {
  const home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String name = 'Mendoza Danny';
    return Scaffold(
      appBar: AppBar(
        title: Text('$name       8voA'),
        centerTitle: true,
      ),
      body: ListScreen(backend: Backend()),
    );
  }
}
