import 'package:api_productostia/models/product.dart';

class Backend {
  /// Singleton pattern used here.
  static final Backend _backend = Backend._internal();

  var products;

  factory Backend() {
    return _backend;
  }

  Backend._internal();

  final _products = [
    Product(
      id: 1,
      nombre: 'Crema dental',
      precio: '1.20',
      descripcion: '-CREMA DENTAL PARA DIENTES SENSIBLES TRIPLE ACCION',
      imagen:
          'https://www.fybeca.com/dw/image/v2/BDPM_PRD/on/demandware.static/-/Sites-masterCatalog_FybecaEcuador/default/dwd4a122c3/images/large/231256_1.jpg?sw=1000&sh=1000',
      cantidad: 1,
    ),
    Product(
      id: 2,
      nombre: 'Papel Higienico Scott',
      precio: '2.00',
      descripcion: '-PAPEL HIGIENICO FAMILIAR TRIPLE HOJA',
      imagen: 'https://www.kcprofessional.cl/media/9702374/30226558.png',
      cantidad: 1,
    ),
    Product(
      id: 3,
      nombre: 'Atun Isabel',
      precio: '1.50',
      descripcion: '-ATUN BAÑADO EN ACEITE VEGETAL',
      imagen:
          'https://yaperito.com/shop/wp-content/uploads/2020/08/atun-isabel_1.jpg',
      cantidad: 1,
    ),
    Product(
      id: 4,
      nombre: 'Agua mineral Tesalia',
      precio: '0.90',
      descripcion: '- Agua mineral ecuatoriana',
      imagen:
          'https://www.fybeca.com/dw/image/v2/BDPM_PRD/on/demandware.static/-/Sites-masterCatalog_FybecaEcuador/default/dwd9dc93f2/images/large/184564-AGUA-SIN-GAS-TESALIA-6000-ML-BOTELLA.jpg?sw=1000&sh=1000',
      cantidad: 1,
    ),
    Product(
      id: 5,
      nombre: 'Jabon Azul Macho',
      precio: '0.50',
      descripcion: '-Jabon azul macho para ropa',
      imagen:
          'https://i0.wp.com/almacenescorsa.com/wp-content/uploads/2021/07/Jabon-El-Macho-Azul-480G.jpg?fit=1280%2C1280&ssl=1',
      cantidad: 1,
    ),
    Product(
      id: 6,
      nombre: 'Shampoo Ego',
      precio: '1.75',
      descripcion: '– Shampoo Masculino formal',
      imagen:
          'https://distribuidorariofrio.com/wp-content/uploads/2022/03/shampoo-ego-force-400ml-1.jpg',
      cantidad: 1,
    ),
    Product(
      id: 7,
      nombre: 'Aceite vegetal Oro',
      precio: '2.00',
      descripcion: 'Aceite esencial de cocina ',
      imagen:
          'https://cdn.shopify.com/s/files/1/0522/1299/0152/products/palma-de-oro-900-ml_grande.jpg?v=1626205515',
      cantidad: 1,
    ),
    Product(
      id: 8,
      nombre: 'Yogurt Echeverria',
      precio: '3.00',
      descripcion: 'Yogurt descremado ideal para el consumo familiar',
      imagen: 'https://www.grupovilaseca.com/images/2018/06/YOGUR_MANGO.jpg',
      cantidad: 1,
    ),
    Product(
      id: 9,
      nombre: 'Leche entera - La lechera',
      precio: '1.00',
      descripcion: 'Leche entera de 1 litro ',
      imagen:
          'https://disprocomecuador.com/wp-content/uploads/2020/10/leche_deslactosada_la_lechera_1_litro.jpg',
      cantidad: 1,
    ),
    Product(
      id: 10,
      nombre: 'Lavaplatos Axion',
      precio: '1.00 c/u',
      descripcion: 'Excelente arrancador de grasa de platos',
      imagen:
          'https://www.merkadomi.com/wp-content/uploads/2020/10/7702010382147.jpg',
      cantidad: 1,
    ),
  ];

  ///
  /// Public API starts here :)
  ///

  /// Returns all products.
  List<Product> getProducts() {
    return _products;
  }

  /// Marks products identified by its id as read.
  void markProductsRead(int id) {
    final index = _products.indexWhere((product) => product.id == id);

    productWidget(product) {}
  }
}
